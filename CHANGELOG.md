# Changelog

## 2.0.3

### Fixed
- memory leak in crate::option::OptionCell

### Added
- crate::option::OptionCell::default

## 2.0.2

### Fixed
- crate::broadcast::cloneable::Reader::clone
- Fixed clippy doc